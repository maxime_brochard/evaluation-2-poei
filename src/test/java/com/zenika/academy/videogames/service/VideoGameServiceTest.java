package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.controllers.VideoGamesController;
import com.zenika.academy.videogames.controllers.representation.VideoGameNameRepresentation;
import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.configuration.IMockitoConfiguration;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class VideoGameServiceTest {

    @InjectMocks //create a class instances which needs to be tested in test class
    private VideoGamesRepository VGRepoTest;

    @Mock //create mock which is needed to support testing of class to be tested
    private RawgDatabaseClient mockRAWG;

    private VideoGamesService VGServiceTest;
    private List<Genre> genreListTest;


    @BeforeEach
    void setUp() {

        this.VGRepoTest = new VideoGamesRepository();
        this.VGServiceTest = new VideoGamesService(VGRepoTest, mockRAWG);
        this.genreListTest = new ArrayList<>();
    }
/*
    @Test
    public void ShouldAddVideoGame(){
        //Arrange
        this.VGServiceTest.addVideoGame("Mario");
        //Assert
        Assertions.assertEquals(1, this.VGServiceTest.ownedVideoGames().size());
    }

 */
}

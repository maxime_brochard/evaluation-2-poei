package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.controllers.VideoGamesController;
import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.service.VideoGamesService;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class videoGamesRepositoryTest {

    private RawgDatabaseClient rawgDatabaseClient;
    private VideoGamesRepository VGRepoTest;
    private VideoGamesService VGServiceTest;
    private List<Genre> genreListTest;


    @BeforeEach
    void setUp() {
        this.rawgDatabaseClient = new RawgDatabaseClient();
        this.VGRepoTest = new VideoGamesRepository();
        this.VGServiceTest = new VideoGamesService(VGRepoTest, rawgDatabaseClient );
        this.genreListTest = new ArrayList<>();

    }


    @Test
    public void ShouldReturnSizeOfRepo(){
        //Arrange
        this.genreListTest.add(new Genre("kart race"));
        VideoGame vgTest = new VideoGame(1L, "Mario", this.genreListTest);

        //Act
        this.VGRepoTest.save(vgTest);

        //Assert
        Assertions.assertEquals(1, VGRepoTest.getAll().size());
        //Assertions.assertEquals("Mario", VGServiceTest.getOneVideoGame(1L).getName());
    }

    @Test
    public void ShouldReturnNameOfRepoFirstVG(){
        //Arrange
        this.genreListTest.add(new Genre("kart race"));
        VideoGame vgTest = new VideoGame(1L, "Mario", this.genreListTest);

        //Act
        this.VGRepoTest.save(vgTest);

        //Assert
        Assertions.assertEquals("Mario", VGServiceTest.getOneVideoGame(1L).getName());
    }
}

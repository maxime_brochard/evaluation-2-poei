package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class VideoGamesService {

    private VideoGamesRepository videoGamesRepository;
    private RawgDatabaseClient rawgDatabaseClient;

    @Autowired
    public VideoGamesService(VideoGamesRepository videoGamesRepository,  RawgDatabaseClient rawgDatabaseClient) {
        this.videoGamesRepository = videoGamesRepository;
        this.rawgDatabaseClient = rawgDatabaseClient;
    }

    public List<VideoGame> ownedVideoGames() {
        //return this.videoGamesRepository.getAll();

        //declare a list of VG to be returned
        List<VideoGame> VGList = new ArrayList<>();

        //get an Optional List of VG from the repository
        List<Optional<VideoGame>> OptionalVGList = this.videoGamesRepository.getAll();

        //For each element of the OptionalVG list, add the element to the returned VG list
        for(long x = 0; x < OptionalVGList.size(); x++) {
            VGList.add(this.videoGamesRepository.get(x).get());
        }

        return VGList;

    }

    public VideoGame getOneVideoGame(Long id) {
        return this.videoGamesRepository.get(id).get();
    }

    public VideoGame addVideoGame(String name) {

        RawgDatabaseClient client = new RawgDatabaseClient();
        VideoGame newGame = client.getVideoGameFromName(name);

        videoGamesRepository.save(newGame);

        return newGame;
    }
}
